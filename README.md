# README #

This README would normally document whatever steps are necessary to get your application up and running.
[Tempat Wisata Di Singapura : tempatwisata.biz.id](http://www.wolesbanget.com/2017/08/tempat-wisata-di-singapura.html)
Pertama, pesawat yg aku gunakan pada mulanya adalah Air Asia, tapi seperti teman-teman ketahui saat ini rute Surabaya - Singapore lagi dibekukan (entah sampai kapan). So, dengan berat hati aku harus melepas tiket murah yg aku dapatkan sebelumnya. Setelah mencari-cari LCC yg lain seperti Tiger atau Jetz, kisaran harga tiket PP adalah 1,7-2 juta. Secara tidak sengaja aku membuka web Garuda airlines sekedar iseng pengen tahu, ternyata malah harganya 145 USD saja (di kurskan ke rupiah saat itu +/- 1,8 juta). Well, murah bingits kalau buat maskapai sekelas Garuda :D Apa saja yg didapat di Garuda? Tentu saja kursi yg lebih lebar, bagasi 30 kg/orang, n makanan lengkap (bukan snack) selama penerbangan. So, tips #1 cek web Garuda, siapa tahu ada promo yg murah hihihi...

Kedua, aku pernah mendengar bahwa membeli tiket masuk tempat wisata secara online lebih murah daripada saat membeli langsung di tempat. Hal ini juga membuat kita tidak perlu antre untuk membeli tiket, sehingga banyak waktu yg bisa di hemat. So, setelah aku browsing, surfing n swimming akhirnya aku menemukan bahwa membeli di travel agent di Singapura bisa jadi jaaauuuuuuhhh lebih murah daripada beli langsung maupun beli online! Sekedar informasi, aku masuk ke adventure cove waterpark + garden by the bay (2 conservatories) plus naik boat quay, hanya 54 SGD per orang, dimana seharusnya bila kita beli tiket langsung kita bisa menghabiskan 87 SGD per orang. So, tips #2 cari travel agent buat beli tiket wisata!

Ketiga, sebagai anak jaman sekarang rasanya kurang afdol kalau internet hp ga nyala hahaha.. setelah aku cari informasi di blog-blog orang, semua menyarankan menggunakan singtel dengan harga beli 15 dollar, n top up pulsa 18 dollar. Beruntungnya aku, adik aku punya perna n hanya perlu 7 dollar buat paket 1 GB selama 7 hari. So, tips #3 cari adik yan punya kartu perna haha.. just joking. Tipsnya adalah kamu bisa pinjam perna teman temanmu yg ga kepake sehingga kamu hanya perlu top up, orrrrr (lucky me, again) waktu beli tiket wisata di sana aku dapat kartu perna gratis. Ini juga OK banget.

Keempat, sudah bukan rahasia lagi kalau makanan di Singapore lumayan mahal. Ini adalah tempat-tempat makan murah yg teman-teman backpacker bisa cari :

1. Changi staff canteen. Di terminal 2, cari pizza hut (di ujung), cari gang kecil n lift yg tersembunyi di sana, trus turun ke lantai B1.

2. Bugis street lurus aja, nyebrang jalan sedikit ada food court besar nan murmer.

3. Di bawah jembatan penyebrangan menuju Vivo city dari stasiun bis n tempat parkir mobil yg mau naik cable car, ada food court juga.

4.Lucky plaza depan plaza ION [Tempat Wisata Di Singapura](http://mtkus.com/tempat-wisata/) di basement ada 2 foodcourt (satu besar, satunya lebih kecil) dengan makanan yg yummmyyy en murraaahh..

5. Lao Pa Sat!! ga mahal kok. Makanannya lengkap juga :D

Tips #5 cek harga hotel lewat travel agent, Agoda atau Traveloka atau Hostelworld, n cek web hotelnya sendiri. Ternyata bisa juga ada selilish harga yg lumayan lho ;)

Last tips and the most important tips : plan, pray and enjoy. Selalu rencanakan, doakan, sisanya nikmati saja. Terkang Tuhan ijinkan sesuatu terjadi di luar rencana, yg ternyata hasilnya jauh lebih baik daripada yg semula kita rencanakan :D

Semoga berguna :D
sumber : [Tempat Wisata Di Singapore](http://www.wolesbanget.com/2016/08/tempat-wisata-di-singapore-yang-paling.html)